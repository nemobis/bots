#!/bin/bash
#
# Extract Twitter IDs from an ORCID data file with xsltproc
# First uncompress the summaries from the tar.gz file
#
# Recklessly, we could also concatenate and pipe the entire XML at once
# with some naive XML rewriting:
# 

wget -O ORCID_2021_10_summaries.tar.gz https://orcid.figshare.com/ndownloader/files/31020067
gzip -dc ORCID_2021_10_summaries.tar.gz | zstd --progress -1c > ORCID_2021_10_summaries.tar.zst
# Uncompress in batches to reduce disk usage even though
# the tar file will be read from start to end 100 times so this is very slow
for i in {0..9}; do
  tar xaf ORCID_2021_10_summaries.tar.zst ORCID_2021_10_summaries/${i}* --checkpoint=500000
  find ORCID_2021_10_summaries -name "*xml" -print0 | xargs -0 -P50 -n1 xsltproc orcid2twitter.xsl | sed --regexp-extended 's,http.+twitter.com/@?([A-Za-z0-9_]+).*,\1,gi' | grep -E "([A-Za-z0-9_]{4,15})$" | tee -a 2021-10_orcid2twitter.tsv
  rm -rf ORCID_2021_10_summaries/
done
