#!/usr/bin/env bash

curl -s "https://fedidb.org/api/v0/network/instances" | jq -r '.[]' | sort | shuf | xargs -P25 -I§ sh -c "curl -s --connect-timeout 3 https://§/api/v1/instance | jq -er 'try . | select(has(\"stats\")) | select(.stats.user_count > 100) | select(has(\"version\")) | select(.version >= \"3.4.0\") | try .uri'" 2>/dev/null | while read -r domain ; do echo "== $domain ==" ; dig +noall +answer +short $domain | tail -n1 | xargs -n1 whois | grep -E "^([Nn]et[Nn]ame|origin)" | sort | tail -n2 ; done
