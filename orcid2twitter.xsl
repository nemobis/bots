<!-- XSLT to print a CSV map from ORCID to Twitter ID from an ORCID data file -->
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:common="http://www.orcid.org/ns/common" xmlns:researcher-url="http://www.orcid.org/ns/researcher-url" xmlns:record="http://www.orcid.org/ns/record">
	<xsl:output method="text" indent="yes" />

	<!-- Just ignore the errors -->
	<xsl:template match="//*[local-name()='error']">
	</xsl:template>

	<!-- If there's an URL named Twitter, print it along with the ORCID -->
	<xsl:template match="//record:record">
		<xsl:if test="//researcher-url:url-name[text()='Twitter']">
			<xsl:apply-templates select="//common:orcid-identifier/common:path"/>
			<xsl:text>	</xsl:text>
			<xsl:apply-templates select="//researcher-url:researcher-url/researcher-url:url-name[text()='Twitter']/../researcher-url:url"/>
			<xsl:text>&#xa;</xsl:text>
		</xsl:if>
	</xsl:template>
</xsl:transform>
