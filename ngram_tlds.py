#!/usr/bin/python
# -*- coding: utf-8  -*-
""" Domain names from bigrams """
#
# (C) Federico Leva, 2022
#
# Distributed under the terms of the MIT license.
#
__version__ = '0.1.1'

import nltk
from nltk.collocations import BigramAssocMeasures
from nltk.corpus import PlaintextCorpusReader
from nltk import BigramCollocationFinder
import re
import requests

tlds = []
# TODO: Consider excluding those not available from OVH/Gandi/whatever
unavailable = ['smile', 'windows','active','amazon','apple','audible',
                     'bank','baseball','basketball','boots','case','drive',
                     'fast','fire','fly','museum','origins','post',
                     'prime','silk','weather'] + \
                    ['arte', 'audi', 'dell', 'gallo', 'globo', 'infiniti',
                     'natura', 'star', 'visa', 'viva', 'vivo', 'va', 'vana'] + \
                    ['data', 'latino', 'mobile'] # Might be upcoming
r = requests.get("https://data.iana.org/TLD/tlds-alpha-by-domain.txt")

for d in r.text.split('\n'):
    if len(d) < 2 or d.startswith("#") or d.startswith("XN"):
        continue
    d = d.lower()
    # Most results are from ['be', 'ci', 'co', 'data', 'li', 're', 'sa', 'si', 'to', 'vi']
    if d not in unavailable and d[-1] in ['a', 'e', 'i', 'o', 'u']:
        tlds.append(d)

tokens = nltk.wordpunct_tokenize(PlaintextCorpusReader(".", ".*", '.txt').raw('YOURCORPUS.txt'))
finder = BigramCollocationFinder.from_words(tokens)

# TODO: better collocation ranking for Italian
for ngram in finder.nbest(BigramAssocMeasures().pmi, 10000):
    for tld in tlds:
        # TODO: Remove diacritics automatically
        if tld and ngram[-1].endswith(tld):
            domain = re.sub(f"{tld}$", f".{tld}", ".".join(list(ngram)))
            # TODO: Skip those which already resolve
            print(domain)
